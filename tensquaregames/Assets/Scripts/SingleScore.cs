﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SingleScore : MonoBehaviour {


    [SerializeField] GameObject[] singleScorePlayer1Array;
    [SerializeField] GameObject[] singleScorePlayer2Array;
    [SerializeField] Text txt_player1TotalScore, txt_player2TotalScore, txt_roundScorePlayer1, txt_roundScorePlayer2, txt_roundNumber;
    [SerializeField] Image winnerBadge, fishImage;
    [SerializeField] Sprite fish1, fish2;

    int player1TotalScore;
    int player2TotalScore;
    int roundScorePlayer1;
    int roundScorePlayer2;
    int roundNumber;

	void Start ()
    {
        roundNumber = 0;
        roundScorePlayer1 = 0;
        roundScorePlayer2 = 0;
        Rematch();   
    }


    int GetRandomSinglePoints()
    {
        int SingleScore = Random.Range(0, 20);
        return SingleScore;
    }

    void RoundScoreUpdate()
    {
        txt_roundScorePlayer1.text = roundScorePlayer1.ToString();
        txt_roundScorePlayer2.text = roundScorePlayer2.ToString();
        txt_roundNumber.text = roundNumber.ToString();

    }


    public void Rematch()
    {
        roundNumber++;
        player1TotalScore = 0;
        player2TotalScore = 0;

        for (var i = 0; i < singleScorePlayer1Array.Length; i++)
        {
            int scoreTemp = GetRandomSinglePoints();
            player1TotalScore += scoreTemp;
            singleScorePlayer1Array[i].GetComponent<Text>().text = scoreTemp.ToString();
            txt_player1TotalScore.text = player1TotalScore.ToString();
        }
        for (var i = 0; i < singleScorePlayer2Array.Length; i++)
        {
            int scoreTemp = GetRandomSinglePoints();
            player2TotalScore += scoreTemp;
            singleScorePlayer2Array[i].GetComponent<Text>().text = scoreTemp.ToString();
            txt_player2TotalScore.text = player2TotalScore.ToString();
        }
        CheckWhoIsTheWinner();
        GetARandomFishImage();
    }


    public void CheckWhoIsTheWinner()
    {
        if (player2TotalScore > player1TotalScore)
        {
            winnerBadge.GetComponent<RectTransform>().localPosition = new Vector2(35f, transform.position.y);
            roundScorePlayer2++;
            RoundScoreUpdate();
            txt_player2TotalScore.color = new Color32(226, 157, 0, 255);
            txt_player1TotalScore.color = new Color32(233, 218, 144, 200);
        }
        else if (player2TotalScore < player1TotalScore)
        {
            winnerBadge.GetComponent<RectTransform>().localPosition = new Vector2(-35f, transform.position.y);
            roundScorePlayer1++;
            RoundScoreUpdate();
            txt_player1TotalScore.color = new Color32(226, 157, 0, 255);
            txt_player2TotalScore.color = new Color32(233, 218, 144, 200);

        }
        else
        {
            winnerBadge.GetComponent<RectTransform>().localPosition = new Vector2(0f, transform.position.y);
            txt_player1TotalScore.color = new Color32(233, 218, 144, 255);
            txt_player2TotalScore.color = new Color32(233, 218, 144, 255);
        }
    }

    void GetARandomFishImage()
    {
        int randFish = Random.Range(0, 2);
        if(randFish == 0)
        {
            fishImage.sprite = fish1;
        }
        else
        {
            fishImage.sprite = fish2;
        }
        
    }

}
